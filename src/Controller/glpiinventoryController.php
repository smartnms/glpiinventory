<?php
/**
 * @file
 * Contains \Drupal\glpi_inventory\Controller.
 */
namespace Drupal\glpiinventory\Controller;
use Drupal\Core\Controller\ControllerBase;
use Drupal\glpiinventory\glpi_api;
use Drupal\Core\StringTranslation;


class glpiinventoryController {
	public function startAction() {
	    $api=new glpi_api();
	    $itemtype='Computer';
	    $testitem=array('name'=>'Test Computer', 'serial'=>'12345');
	    $createoutput=$api->createitem($itemtype,$testitem);
	    $id=$createoutput['id'];
	    $loadoutput=$api->getitem($itemtype,$id,array('expand_dropdowns'=>'true'));
        $updateoutput=$api->updateitem($itemtype,$id,$testitem);
        $deleteoutput=$api->deleteitem($itemtype,$id);

        $rows=array();
        foreach($createoutput as $key => $value)
            $rows[]=[$key,$value];

	    $output['create']=array('#type'=>'details',
            '#title' => t('Creation Test'),
            '#description' => t('Result of creation request'),
            '#open' => TRUE,);
        $output['create']['raw']=array('#markup'=>print_r($createoutput,TRUE),'#title'=>t('Raw output'));
        $output['create']['processed']=array('#type' =>'table',
            '#title' => t('Create Output'),
            '#header'=>array(t('Key'),t('Value')),
            '#rows' => $rows);

        $rows=array();
        foreach($loadoutput as $key => $value)
            $rows[]=[$key,$value];


        $output['load']=array('#type'=>'details',
            '#title' => t('Load Test'),
            '#description' => t('Result of load request'),
            '#open' => TRUE,);
        $output['load']['raw']=array('#markup'=>print_r($loadoutput,TRUE),'#title'=>t('Raw output'));
        $output['load']['processed']=array('#type' =>'table',
            '#title' => t('Create Output'),
            '#header'=>array(t('Key'),t('Value')),
            '#rows' => $rows);

        $rows=array();
        foreach($updateoutput as $key => $value)
            $rows[]=[$key,$value];

        $output['update']=array('#type'=>'details',
            '#title' => t('Update Test'),
            '#description' => t('Result of update request'),
            '#open' => TRUE,);
        $output['update']['raw']=array('#markup'=>print_r($updateoutput,TRUE),'#title'=>t('Raw output'));
        $output['update']['processed']=array('#type' =>'table',
            '#title' => t('Create Output'),
            '#header'=>array(t('Key'),t('Value')),
            '#rows' => $rows);

        $rows=array();
        foreach($deleteoutput as $key => $value)
            $rows[]=[$key,$value];

        $output['delete']=array('#type'=>'details',
            '#title' => t('Delete Test'),
            '#description' => t('Result of delete request'),
            '#open' => TRUE,);
        $output['delete']['raw']=array('#markup'=>print_r($deleteoutput,TRUE),'#title'=>t('Raw output'));
        $output['delete']['processed']=array('#type' =>'table',
            '#title' => t('Delete Output'),
            '#header'=>array(t('Key'),t('Value')),
            '#rows' => $rows);
		return $output;
	}

	public function settings() {
		return [
			'#markup' => '<h2>Página de gestión de GLPI</h2>',
		];
	}
}
