<?php

namespace Drupal\glpiinventory;
use Drupal\Core\Logger;
use Drupal\Core\Form\ConfigFormBase;
use GuzzleHttp\Exception\RequestException;

/**
 * @brief   Concrete class for the GLPI API.
 */

class glpi_api
{

    public $client;
    public $tempstore;
    private $user;
    private $password;
    private $key;
    private $userkey;
    private $printCommunication = true;
    private $apiUrl = '';


    /**
     * @brief   Returns the API url for all requests.
     *
     * @retval  string  API url.
     */

    public function getApiUrl()
    {
        return $this->apiUrl;
    }

    /**
     * @brief   Sets the API url for all requests.
     *
     * @param   $apiUrl     API url.
     *
     * @retval  glpi_api
     */

    public function setApiUrl($apiUrl)
    {
        $this->apiUrl = $apiUrl;
        return $this;
    }


    public function __construct()
    {
        $config = \Drupal::config('glpiinventory.settings');
        $this->client = \Drupal::httpClient();
        $this->tempstore = \Drupal::service('user.private_tempstore')->get('glpiinventory');
        $apiUrl = $config->get('glpi_api_url');
        $this->user = $config->get('glpi_api_user');
        $this->password = $config->get('glpi_api_pass');
        $this->key = $config->get('glpi_api_key');
        $this->userkey = $config->get('glpi_user_key');
        $this->printCommunication=$config->get('glpi_api_debug');
        if ($apiUrl)
            $this->setApiUrl($apiUrl);
        if($this->tempstore->get('session_token')==NULL)
            $this->tempstore->set('session_token',$this->createtoken());
    }

    public function createtoken()
    {

        // build request url
        $url = $this->getapiUrl().'/initSession';

        // encode request array
        $this->request['url']=$url;
        $this->request['params']=array(
            'headers' => array('App-Token'=>$this->key,
                'Content-Type' => 'application/json',
            'Authorization' => 'user_token '.$this->userkey));
        // debug logging
        if($this->printCommunication)
            debug('Request: '.print_r($this->request,true), 'glpiinventory');

        // get response
        $this->response = $this->client->get($this->request['url'],$this->request['params']);

        // response verification

        if($this->response === FALSE)
            throw new Exception('Could not read data from "'.$url.'"');

        // decode response
        $this->responseDecoded = json_decode($this->response->getBody(),true);
        // debug logging
        if($this->printCommunication)
            debug('Response: '.print_r($this->responseDecoded,true), 'glpiinventory');
        return $this->responseDecoded['session_token'];
    }

    public function getitem($itemtype='computer', $item_id, $query)
    {

        // build request url
        $url = $this->getapiUrl().'/'.$itemtype.'/'.$item_id;

        // encode request array
        $this->request['url']=$url;
        $this->request['params']=array(
            'headers' => array('Session-Token'=>$this->tempstore->get('session_token'),'Content-Type' => 'application/json','App-Token'=>$this->key),
                'query' => $query);

        // debug logging
        if($this->printCommunication)
            debug('Request: '.print_r($this->request,true), 'glpiinventory');

        // get response
        try
        {
        $this->response = $this->client->get($this->request['url'],$this->request['params']);
        }
        catch (RequestException $e) {
            \Drupal::logger('glpiinventory')->error('Exception: '.$e->getMessage());
            $response=$e->getResponse();
            $code=$response->getStatusCode();
            if($code=='401'){
                $this->tempstore->delete('session_token');
                $this->tempstore->set('session_token',$this->createtoken());
                $this->response = $this->client->get($this->request['url'],$this->request['params']);
            }
        }
        // response verification
        if($this->response === FALSE)
            throw new Exception('Could not read data from "'.$url.'"');

        // decode response
        $this->responseDecoded = json_decode($this->response->getBody(),true);
        // debug logging
        if($this->printCommunication)
            debug('Response: '.print_r($this->responseDecoded,true), 'glpiinventory');
        return $this->responseDecoded;
    }

    public function createitem($itemtype='computer', $params)
    {

        // build request url
        $url = $this->getapiUrl().'/'.$itemtype.'/';

        // encode request array
        $this->request['url']=$url;
        $this->request['params']=array(
            'headers' => array('Session-Token'=>$this->tempstore->get('session_token'),'Content-Type' => 'application/json','App-Token'=>$this->key));


        $this->body['input'] = $params;

        // encode request array
        $this->bodyEncoded = json_encode($this->body);
        $this->request['params']['body']=$this->bodyEncoded;

        // debug logging
        if($this->printCommunication)
            debug('Request: '.print_r($this->request,true), 'glpiinventory');

        // get response
        try
        {
        $this->response = $this->client->post($this->request['url'],$this->request['params']);
        }
        catch (RequestException $e) {
             $response = $e->getResponse();
            $code = $response->getStatusCode();
            \Drupal::logger('glpiinventory')->error('Exception: ' . $e->getMessage().' Code:'.$code);
            if ($code == '401') {
                $this->tempstore->delete('session_token');
                $this->tempstore->set('session_token', $this->createtoken());
                $this->response = $this->client->post($this->request['url'], $this->request['params']);
            }
        }
        // response verification
        if($this->response === FALSE)
            throw new Exception('Could not read data from "'.$url.'"');

        // decode response
        $this->responseDecoded = json_decode($this->response->getBody(),true);
        // debug logging
        if($this->printCommunication)
            debug('Response: '.print_r($this->responseDecoded,true), 'glpiinventory');
        return $this->responseDecoded;
    }

    public function updateitem($itemtype='computer', $item_id, $params)
    {

        // build request url
        $url = $this->getapiUrl().'/'.$itemtype.'/'.$item_id;

        // encode request array
        $this->request['url']=$url;
        $this->request['params']=array(
            'headers' => array('Session-Token'=>$this->tempstore->get('session_token'),'Content-Type' => 'application/json','App-Token'=>$this->key));


        $this->body['input'] = $params;

        // encode request array
        $this->bodyEncoded = json_encode($this->body);
        $this->request['params']['body']=$this->bodyEncoded;

        // debug logging
        if($this->printCommunication)
            debug('Request: '.print_r($this->request,true), 'glpiinventory');

        // get response
        try
        {
            $this->response = $this->client->put($this->request['url'],$this->request['params']);
        }
        catch (RequestException $e) {
            $response = $e->getResponse();
            $code = $response->getStatusCode();
            \Drupal::logger('glpiinventory')->error('Exception: ' . $e->getMessage().' Code:'.$code);
            if ($code == '401') {
                $this->tempstore->delete('session_token');
                $this->tempstore->set('session_token', $this->createtoken());
                $this->response = $this->client->put($this->request['url'], $this->request['params']);
            }
        }
        // response verification
        if($this->response === FALSE)
            throw new Exception('Could not read data from "'.$url.'"');

        // decode response
        $this->responseDecoded = json_decode($this->response->getBody(),true);
        // debug logging
        if($this->printCommunication)
            debug('Response: '.print_r($this->responseDecoded,true), 'glpiinventory');
        return $this->responseDecoded;
    }

    public function deleteitem($itemtype='computer', $item_id)
    {

        // build request url
        $url = $this->getapiUrl().'/'.$itemtype.'/'.$item_id;

        // encode request array
        $this->request['url']=$url;
        $this->request['params']=array(
            'headers' => array('Session-Token'=>$this->tempstore->get('session_token'),'Content-Type' => 'application/json','App-Token'=>$this->key));


        // encode request array
        $this->bodyEncoded = json_encode($this->body);

        // debug logging
        if($this->printCommunication)
            debug('Request: '.print_r($this->request,true), 'glpiinventory');

        // get response
        try
        {
            $this->response = $this->client->delete($this->request['url'],$this->request['params']);
        }
        catch (RequestException $e) {
            $response = $e->getResponse();
            $code = $response->getStatusCode();
            \Drupal::logger('glpiinventory')->error('Exception: ' . $e->getMessage().' Code:'.$code);
            if ($code == '401') {
                $this->tempstore->delete('session_token');
                $this->tempstore->set('session_token', $this->createtoken());
                $this->response = $this->client->delete($this->request['url'], $this->request['params']);
            }
        }
        // response verification
        if($this->response === FALSE)
            throw new Exception('Could not read data from "'.$url.'"');

        // decode response
        $this->responseDecoded = json_decode($this->response->getBody(),true);
        // debug logging
        if($this->printCommunication)
            debug('Response: '.print_r($this->responseDecoded,true), 'glpiinventory');
        return $this->responseDecoded;
    }
}
?>