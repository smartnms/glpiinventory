<?php

namespace Drupal\glpiinventory\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure glpi_inventory settings for this site.
 */
class glpiinventorySettingsForm extends ConfigFormBase {
  /** 
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'glpi_inventory_admin_settings';
  }

  /** 
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'glpiinventory.settings',
    ];
  }

  /** 
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('glpiinventory.settings');

    $form['glpi_inventory_apiurl'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('API connection URL'),
      '#default_value' => $config->get('glpi_api_url'),
    );
      $form['glpi_api_user'] = array(
          '#type' => 'textfield',
          '#title' => $this->t('User for API connection'),
          '#default_value' => $config->get('glpi_api_user'),
      );

      $form['glpi_api_pass'] = array(
          '#type' => 'password',
          '#title' => $this->t('Password for the User in API connection'),
          '#default_value' => $config->get('glpi_api_pass'),
      );

      $form['glpi_api_key'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('API Connection key'),
      '#default_value' => $config->get('glpi_api_key'),
    );
      $form['glpi_user_key'] = array(
          '#type' => 'textfield',
          '#title' => $this->t('User API Token'),
          '#default_value' => $config->get('glpi_user_key'),
      );
      $form['glpi_api_debug'] = array(
          '#type' => 'checkbox',
          '#title' => $this->t('Enable debug mode'),
          '#default_value' => $config->get('glpi_api_debug'),
      );

      return parent::buildForm($form, $form_state);
  }

  /** 
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration
    $this->config('glpiinventory.settings')
      // Set the submitted configuration setting
      ->set('glpi_api_url', $form_state->getValue('glpi_inventory_apiurl'))
      // You can set multiple configurations at once by making
      // multiple calls to set()
      ->set('glpi_api_key', $form_state->getValue('glpi_api_key'))
      ->set('glpi_user_key', $form_state->getValue('glpi_user_key'))
      ->set('glpi_api_user', $form_state->getValue('glpi_api_user'))
      ->set('glpi_api_pass', $form_state->getValue('glpi_api_pass'))
      ->set('glpi_api_debug', $form_state->getValue('glpi_api_debug'))
      ->save();

    parent::submitForm($form, $form_state);
  }
}


?>